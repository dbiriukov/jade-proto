package controllers

import model.Test
import play.api.mvc._

object Application extends Controller {

  def index = Action {
//    Ok(views.html.index("Your new application is ready."))
    Ok(Scalate("index.jade").render('title -> "Hello, world!", Symbol("ololo") -> Test("12345")))
  }

}